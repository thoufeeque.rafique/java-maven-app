import gspread


def testing_challenge(name, code, comment):

    gc = gspread.service_account()
    ws = gc.open("Testing challenge").sheet1

    all_values = ws.get_all_values()
    for i, row in enumerate(all_values[1:], 2):
        print(i, row)
        if not any(row[2:]):
            print(i, row)
            break

    ws.update(f'c{i}:e{i}', [[name, code, comment]])
    
